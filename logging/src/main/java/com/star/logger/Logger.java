package com.star.logger;

import com.star.LevelManager.Level;

public abstract class Logger {

    public void info(String msg){
        Level.valueOf("INFO").getStrategy().execute(msg);
    }

    public void error(String msg){
        Level.valueOf("ERROR").getStrategy().execute(msg);
    }

    public void debug(String msg){
        Level.valueOf("DEBUG").getStrategy().execute(msg);
    }
}
