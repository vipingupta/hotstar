package com.star.logger;

import com.star.Outputmanager.Output;
import com.star.Outputmanager.impl.ConsoleLogger;
import com.star.Outputmanager.impl.FileLogger;
import com.star.LevelManager.Level;
import com.star.Outputmanager.OutputStratgy;

import java.io.*;
import java.util.Properties;

public class LogManager {

    public OutputStratgy outputStratgy;



    public Level level;

    Properties prop = new Properties();
    InputStream input = null;
    public LogManager() {
        try {

            input = new FileInputStream("config.properties");
            prop.load(input);

            outputStratgy = Output.valueOf(prop.getProperty("output").toUpperCase()).getOutputStratgy();


            level = Level.valueOf(prop.getProperty("level").toUpperCase());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public OutputStratgy getOutputStratgy(){

        return this.outputStratgy;
    }


    public Level getLevel() {
        return this.level;
    }


}
