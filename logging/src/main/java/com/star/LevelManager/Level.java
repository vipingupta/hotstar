package com.star.LevelManager;

import com.star.LevelManager.Impl.DebugLog;
import com.star.LevelManager.Impl.ErrorLog;
import com.star.LevelManager.Impl.Infolog;

public enum Level {
    INFO(new Infolog()),
    DEBUG(new DebugLog()),
    ERROR(new ErrorLog());

    private ProcessStrategy processStrategy;
    Level(ProcessStrategy processStrategy) {
        this.processStrategy = processStrategy;
    }

    public ProcessStrategy getStrategy() {
        return this.processStrategy;
    }
}
