package com.star.LevelManager;

import com.star.logger.LogManager;

public interface ProcessStrategy {

    void execute(String msg);

    LogManager logManager = new LogManager();
}
