package com.star.LevelManager.Impl;

import com.star.LevelManager.Level;
import com.star.LevelManager.ProcessStrategy;

public class DebugLog implements ProcessStrategy {


    public void execute(String msg) {

        if (logManager.getLevel()==Level.DEBUG) {
            logManager.getOutputStratgy().write("[DEBUG] " + msg);
        }
    }
}