package com.star.LevelManager.Impl;

import com.star.LevelManager.Level;
import com.star.LevelManager.ProcessStrategy;

public class ErrorLog implements ProcessStrategy {

    public void execute(String msg) {
        if (logManager.getLevel()==Level.ERROR) {
            logManager.getOutputStratgy().write("[ERROR] " + msg);
        }
    }
}