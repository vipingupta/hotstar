package com.star.LevelManager.Impl;

import com.star.LevelManager.Level;
import com.star.LevelManager.ProcessStrategy;

public class Infolog implements ProcessStrategy {
    public void execute(String msg) {

        if (logManager.getLevel()==Level.INFO){
            logManager.getOutputStratgy().write("[INFO] "+msg);
        }
    }
}
