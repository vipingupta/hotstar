package com.star.Outputmanager;

import com.star.Outputmanager.impl.ConsoleLogger;
import com.star.Outputmanager.impl.FileLogger;

public enum Output {
    FILE(new FileLogger()),
    CONSOLE(new ConsoleLogger());

    private OutputStratgy outputStratgy;

    Output(OutputStratgy outputStratgy) {
        this.outputStratgy = outputStratgy;
    }

    public OutputStratgy getOutputStratgy() {
        return this.outputStratgy;
    }
}
