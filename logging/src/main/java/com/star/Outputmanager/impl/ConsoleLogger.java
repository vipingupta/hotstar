package com.star.Outputmanager.impl;

import com.star.Outputmanager.OutputStratgy;
import com.star.Utility.Common;

public class ConsoleLogger implements OutputStratgy {

    public synchronized void write(String message) {
        System.out.println("console output: " + Common.getCurrenttime() +" "+ message);
    }
}
