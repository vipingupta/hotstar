package com.star.Outputmanager.impl;

import com.star.Outputmanager.OutputStratgy;
import com.star.Utility.Common;

public class FileLogger implements OutputStratgy {

    public synchronized void write(String message) {
        System.out.println("file output: "+ Common.getCurrenttime() +" "+message);
    }
}
